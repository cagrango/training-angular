import { Directive, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { UtilService } from '../core/util.service';

@Directive({
  selector: '[CurrencyInput]'
})
export class CurrencyInputDirective {

  originalAmount : number | undefined;

  @Input()
  get OriginalAmount(): number | undefined { return this.originalAmount; }
  set OriginalAmount(value: number | undefined) {
    this.originalAmount = value;
  }

  @Output() OriginalAmountChange = new EventEmitter<number>();

  constructor(private el: ElementRef,
    private utilService: UtilService) { }

  ngOnChanges() {
    this.setValue();
  }

  setValue() {
    if( this.originalAmount ) {
      this.el.nativeElement.value = this.utilService.transformAmount(this.originalAmount);
    }
    else {
      this.el.nativeElement.value = ''
    }
  }

  @HostListener('focus')
  onFocusIn() {
    if( this.originalAmount ) {
      this.el.nativeElement.value = this.originalAmount;
    }
    else {
      this.el.nativeElement.value = ''
    }
  }

  @HostListener('focusout')
  onBlur() {
    let stringToConvert = this.el.nativeElement.value;
    if ( stringToConvert == '' ) {
      this.originalAmount = undefined;
    }
    else if (!isNaN(Number(stringToConvert))) {
      this.originalAmount = Number(stringToConvert);
    }
    this.setValue();
    this.OriginalAmountChange.emit(this.originalAmount);
  }

}