import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreditListComponent } from './credit-list/credit-list.component'
import { CreditCreateComponent } from './credit-create/credit-create.component';
import { CreditEditComponent } from './credit-edit/credit-edit.component';

const routes: Routes = [ 
  { path: '' , component: CreditListComponent} ,
  { path: 'create' , component: CreditCreateComponent},
  { path: 'edit/:id', component: CreditEditComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
