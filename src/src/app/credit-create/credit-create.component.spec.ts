import { HttpClient } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { CreditCreateComponent } from './credit-create.component';

describe('CreditCreateComponent', () => {
  let component: CreditCreateComponent;
  let fixture: ComponentFixture<CreditCreateComponent>;
  let httpClientSpy: { get:jasmine.Spy; post: jasmine.Spy; put:jasmine.Spy; };

  beforeEach(async(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get','post','put']);
    TestBed.configureTestingModule({
      declarations: [ CreditCreateComponent ],
      imports: [RouterTestingModule],
      providers : [
        { provide: HttpClient, useValue: httpClientSpy }
      ] 
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
