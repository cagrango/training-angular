import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CreditService } from '../core/credit.service';
import { CreditBaseComponent } from '../credit-base/credit-base.component';
import { ICredit } from '../interfaces/credit.interface';

@Component({
  selector: 'app-credit-create',
  templateUrl: './credit-create.component.html',
  styleUrls: ['./credit-create.component.css']
})
export class CreditCreateComponent implements OnInit {

  @ViewChild('creditBase') creditBase;

  constructor(private router : Router,
              private creditService : CreditService) { }

  ngOnInit(): void {
  }

  create() {
    var control = this.creditBase as CreditBaseComponent;
    if( !control.isValid() ) {
      window.alert('Formulario no valido');
      return;
    }
    let credit = control.credit;
    this.creditService.create( credit ).then( data => {
      window.alert('El credito fue creado con identificador: ' + data.id );
      this.router.navigateByUrl('/');
    });
  }

  cancel() {
    this.router.navigateByUrl('/');
  }

}
