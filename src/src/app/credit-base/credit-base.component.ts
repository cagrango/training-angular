import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';

import { ICredit } from '../interfaces/credit.interface';
import { INTEREST } from '../core/interest-mock';
import { CreditService } from '../core/credit.service';
import { UtilService } from '../core/util.service';
import { OnlyNumberInputDirective } from '../directives/only-number-input.directive'
import { CurrencyInputDirective } from '../directives/currency-input.directive'

@Component({
  selector: 'app-credit-base',
  templateUrl: './credit-base.component.html',
  styleUrls: ['./credit-base.component.css']
})
export class CreditBaseComponent implements OnInit {

  interests = INTEREST
  monthQuota = 0
  creditId = 0
  editMode = false;
  originalAmount : number | undefined = undefined;

  creditForm = this.formBuilder.group({
    customerName: '',
    amount: '',
    term: '',
    interestPercent: ''
  });

  @Input()
  get credit(): ICredit { return this.readFromForm(); }
  set credit(request: ICredit | undefined) {
    if( request != undefined ) {        
      this.creditId = request.id;
      this.originalAmount = request.amount;
      this.creditForm.patchValue({
        customerName: request.customerName,
        term: request.term,
        interestPercent: request.interestPercent
      });
      this.onChange();
    }
  }

  @Input()
  set EditMode ( value : boolean | undefined ) {
    if( value == true ) {
      let control = this.creditForm.get('amount')
      control.disable();
    }
    else {
      this.creditForm.patchValue({
        interestPercent: 0
      });  
    }
  }

  constructor(private formBuilder: FormBuilder,
              private creditService : CreditService,
              private utilService : UtilService) { }

  ngOnInit(): void { 
  }

  isValid() {
    var credit = this.readFromForm();
    if( credit.customerName != '' &&
        credit.amount > 0 &&
        credit.term > 0 &&
        credit.interestPercent > 0
    ) {
      return true;
    }
    else {
      return false;
    }
  }

  readFromForm() {
    let response: ICredit = {
      customerName : this.creditForm.get('customerName').value,
      amount : this.originalAmount,
      interestPercent : Number(this.creditForm.get('interestPercent').value),
      term : Number(this.creditForm.get('term').value),
      id : this.creditId,
      monthQuota : 0,
    };
    return response;
  }

  onChange() {
    if( this.isValid() ) {
      this.creditService.calculate( this.readFromForm() ).then( data => {
        this.monthQuota = data.monthQuota;
      });
    }
  }

  transformAmount(value : number) {
    return this.utilService.transformAmount( value );
  }

}
