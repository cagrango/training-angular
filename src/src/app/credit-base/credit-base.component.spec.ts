import { CurrencyPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

import { CreditBaseComponent } from './credit-base.component';

describe('CreditBaseComponent', () => {
  let component: CreditBaseComponent;
  let fixture: ComponentFixture<CreditBaseComponent>;
  let httpClientSpy: { get:jasmine.Spy; post: jasmine.Spy; put:jasmine.Spy; };

  beforeEach(async(() => {
    
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get','post','put']);

    TestBed.configureTestingModule({
      declarations: [ CreditBaseComponent ],
      imports: [RouterTestingModule],
      providers: [ 
         FormBuilder,
         CurrencyPipe,
         { provide: HttpClient, useValue: httpClientSpy }
         ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
