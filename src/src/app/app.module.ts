import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule, CurrencyPipe} from '@angular/common';
import { OnlyNumberInputDirective } from './directives/only-number-input.directive'
import { CurrencyInputDirective } from './directives/currency-input.directive'

import { CreditListComponent } from './credit-list/credit-list.component';
import { CreditCreateComponent } from './credit-create/credit-create.component';
import { CreditBaseComponent } from './credit-base/credit-base.component';
import { CreditEditComponent } from './credit-edit/credit-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    CreditListComponent,
    CreditCreateComponent,
    CreditBaseComponent,
    CreditEditComponent,
    OnlyNumberInputDirective,
    CurrencyInputDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [CurrencyPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
