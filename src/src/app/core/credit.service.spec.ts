import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { CreditService } from './credit.service';
import { SecurityService } from './security.service';

describe('CreditService', () => {
  let service: CreditService;
  let httpClientSpy: { get:jasmine.Spy; post: jasmine.Spy; put:jasmine.Spy; };

  beforeEach(() => {
    
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get','post','put']);

    TestBed.configureTestingModule({ 
      providers : [        
        { provide: HttpClient, useValue: httpClientSpy }
      ] 
    });
    service = TestBed.inject(CreditService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
