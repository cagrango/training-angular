import { IInterest } from '../interfaces/interest.interface'

export const INTEREST : IInterest [] = [ 
    { id : 0 , text : 'Seleccionar' }, 
    { id : 0.85 , text : '0,85 %' }, 
    { id : 1 , text : '1 %' }, 
    { id : 1.25 , text : '1,25 %' }, 
    { id : 2 , text : '2 %' }, 
    { id : 2.4 , text : '2,4 %' }
];