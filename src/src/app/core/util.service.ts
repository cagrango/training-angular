import { Injectable } from '@angular/core';
import { CommonModule, CurrencyPipe} from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor(private currencyPipe : CurrencyPipe) { }

  transformAmount(value : number) {
    return this.currencyPipe.transform(value,'$ ','symbol','1.0-0');
  }
  
}
