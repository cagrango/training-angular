import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ICredit } from '../interfaces/credit.interface';
import { SecurityService } from './security.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class CreditService {

  credits : ICredit [] = [];

  constructor(private http: HttpClient,
    private securityService: SecurityService) { }

  async getAll() {
    return await this.securityService.getHeaders().then( data => {      
      let observable = this.http.get<ICredit[]> 
      ( environment.apiUrl + '/credit' ,
        data
      )  
      observable.subscribe( data => {
        this.credits = data;
      });  
      return observable.toPromise();
    });
  }

  getCreditFromCache( id : number ) {
    let credit = this.credits.find(x => x.id == id);
    return credit;
  }

  async calculate( credit : ICredit ) {
    return await this.securityService.getHeaders().then( data => {
      return this.http.post<ICredit>( 
      environment.apiUrl + '/credit/calculate',
      credit,
      data ).toPromise();
    });
  }

  async create( credit : ICredit ) {
    return await this.securityService.getHeaders().then( data => {
      return this.http.post<ICredit>( 
        environment.apiUrl + '/credit',
        credit,
        data ).toPromise();
    });
  }

  async edit( credit : ICredit ) {
    return await this.securityService.getHeaders().then( data => {
      return this.http.put<ICredit>( 
        environment.apiUrl + '/credit',
        credit,
        data ).toPromise();
    });
  }

}
