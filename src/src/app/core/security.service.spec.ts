import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { SecurityService } from './security.service';

describe('SecurityService', () => {
  
  let service: SecurityService;
  let httpClientSpy: { post: jasmine.Spy; };

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['post']);
    
    TestBed.configureTestingModule({
      providers: [
        SecurityService,
        { provide: HttpClient, useValue: httpClientSpy }
      ]
    });
    
    service = TestBed.inject(SecurityService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
