import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ILoginRequest } from '../interfaces/login.request.interface';
import { ILoginResponse } from '../interfaces/login.response.interface';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  loginResponse : ILoginResponse | undefined = undefined 

  constructor(private http: HttpClient) { }

  authenticate() {    
    let httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    let loginRequest : ILoginRequest = {
      login : "default",
      password : "default"
    };
    let observable = this.http.post<ILoginResponse>( 
      environment.apiUrl + '/login',
      loginRequest,
      httpOptions);
    return observable;
  }

  async getHeaders() {
    if( this.loginResponse === undefined ) {
      await this.authenticate().toPromise().then( data => { 
        this.loginResponse = data
      });
    }
    let token = 'Bearer ' + this.loginResponse.token;
    let httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json' , 'Authorization': token})
    }
    return httpOptions;
  }  

}
