import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CreditService } from '../core/credit.service';
import { SecurityService } from '../core/security.service';
import { UtilService } from '../core/util.service';
import { ICredit } from '../interfaces/credit.interface';

@Component({
  selector: 'app-credit-list',
  templateUrl: './credit-list.component.html',
  styleUrls: ['./credit-list.component.css']
})
export class CreditListComponent implements OnInit {

  credits : ICredit [] = [];

  constructor( 
    private securityService : SecurityService,
    private creditService : CreditService,
    private router : Router,
    private utilService : UtilService ) { }

  ngOnInit(): void {
      this.creditService.getAll().then( data => {         
          this.credits = data;
      });
  }
  
  createCredit() {
    this.router.navigateByUrl('/create');
  }
  
  transformAmount(value : number) {
    return this.utilService.transformAmount( value );
  }

}
