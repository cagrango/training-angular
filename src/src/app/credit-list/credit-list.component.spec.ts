import { CurrencyPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { CreditListComponent } from './credit-list.component';

describe('CreditListComponent', () => {
  let component: CreditListComponent;
  let fixture: ComponentFixture<CreditListComponent>;
  let httpClientSpy: { get:jasmine.Spy; post: jasmine.Spy; put:jasmine.Spy; };

  beforeEach(async(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get','post','put']);
    TestBed.configureTestingModule({
      declarations: [ CreditListComponent ],
      imports: [RouterTestingModule],
      providers : [        
        CurrencyPipe,        
        { provide: HttpClient, useValue: httpClientSpy }
      ] 
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
