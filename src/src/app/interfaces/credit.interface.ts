export interface ICredit {
    id : number;
    customerName : string;
    amount : number;
    term : number;
    interestPercent : number;    
    monthQuota : number;
}