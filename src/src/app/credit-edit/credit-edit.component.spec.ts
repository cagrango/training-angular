import { HttpClient } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { CreditEditComponent } from './credit-edit.component';

describe('CreditEditComponent', () => {
  let component: CreditEditComponent;
  let fixture: ComponentFixture<CreditEditComponent>;
  let httpClientSpy: { get:jasmine.Spy; post: jasmine.Spy; put:jasmine.Spy; };
  
  beforeEach(async(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get','post','put']);
    TestBed.configureTestingModule({
      declarations: [ CreditEditComponent ],
      imports: [RouterTestingModule],
      providers : [
        { provide: HttpClient, useValue: httpClientSpy }
      ]       
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
