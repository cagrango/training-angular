import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CreditService } from '../core/credit.service';
import { CreditBaseComponent } from '../credit-base/credit-base.component';
import { ICredit } from '../interfaces/credit.interface';

@Component({
  selector: 'app-credit-edit',
  templateUrl: './credit-edit.component.html',
  styleUrls: ['./credit-edit.component.css']
})
export class CreditEditComponent implements OnInit {

  @ViewChild('creditBase') creditBase;
  
  constructor(private router : Router,
              private creditService : CreditService,
              private activatedRoute: ActivatedRoute ) { }

  ngOnInit(): void {
    const routeParams = this.activatedRoute.snapshot.paramMap;
    const IdFromRoute = Number(routeParams.get('id'));
    const credit = this.creditService.getCreditFromCache(IdFromRoute);
    this.credit = credit;
  }

  credit : ICredit | undefined = undefined;

  save() {
    var control = this.creditBase as CreditBaseComponent;
    if( !control.isValid() ) {
      window.alert('Formulario no valido');
      return;
    }
    let credit = control.credit;
    this.creditService.edit( credit ).then( data => {
      window.alert('El credito fue editado correctamente' );
      this.router.navigateByUrl('/');
    });
  }

  cancel() {
    this.router.navigateByUrl('/');
  }  

}
